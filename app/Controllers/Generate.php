<?php namespace App\Controllers;

use CodeIgniter\HTTP\IncomingRequest;
use App\Models\RequestModel;
use App\Models\AuthModel;
use App\Models\HistoryModel;
use \Firebase\JWT\JWT;

// QR Code Components
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class Generate extends BaseController
{
    public function __construct(){
        //Models
        $this->reqModel = new RequestModel();
        $this->authModel = new AuthModel();
        $this->historyModel = new HistoryModel();
    }

    public function downloadTest(){

        $data = $this->request->getJSON();
        $downloadLink = base_url("index.php/mlrs/api/v1/generate/testResult/". $data->appId);

        $response = [
            "urlLink" =>  $downloadLink
        ];
        
        return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));

    }

    public function getTestResult($id){
        
        //Data Gather 
        $query = $this->reqModel->getDetails(["id" => $id]);
        $query->requisitioner = $this->reqModel->getRequisitioner(["id" => $query->createdBy]);

        $performer = [];
        $approver = [];
        $process = explode(",", $query->performedBy);
        $approve = explode(",", $query->approveBy);
        foreach($process as $key => $value) {
            $qry = $this->reqModel->getUserSignature(["id" => $value]);
            array_push($performer, $qry);
        }
        foreach($approve as $key => $value) {
            $aqry = $this->reqModel->getUserSignature(["id" => $value]);
            array_push($approver, $aqry);
        }

        $query->performedBy = $performer;
        $query->approveBy = $approver;

        $query->verifiedBy = $this->reqModel->getUserSignature(["id" => $query->verifiedBy]);
        $query->encodedBy = $this->reqModel->getUserSignature(["id" => $query->encodedBy]);

        // echo "<pre>";
        // print_r($query);
        // exit();

        // QR Code Implementation
        $writer = new PngWriter();
        // Create QR code
        // $qrLink = "http://47.91.24.159/search/scan/". $query->referenceId;
        $qrLink = base_url("index.php/mlrs/api/v1/verify/qr/testResult/". $id);
        // print_r($qrLink);
        // exit();
        $qrCode = QrCode::create($qrLink)
        ->setEncoding(new Encoding('UTF-8'))
        ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
        ->setSize(300)
        ->setMargin(10)
        ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
        ->setForegroundColor(new Color(0, 0, 0))
        ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);

        // header('Content-Type: '.$result->getMimeType());
        // echo $result->getString();
        // echo "<br/>";

        $qrUri = $result->getDataUri();

        $query->qrCode = $qrUri;

        // Generate the PDF
        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml(view('test_result/result', (array)$query));
        $dompdf->setPaper('Legal', 'portrait');
        $dompdf->render();
        $dompdf->stream($query->referenceId .'.pdf', ["Attachment" => true]);

        

    }

    public function getVerifyTestResultQRCode($id){
        //Data Gather 
        $query = $this->reqModel->getDetails(["id" => $id]);

        // echo "<pre>";
        // print_r($query);
        // exit();

        return view('test_result/verification', (array)$query);
        // Generate the PDF
        // $dompdf = new \Dompdf\Dompdf(); 
        // $dompdf->loadHtml(view('test_result/verification', (array)$query));
        // $dompdf->setPaper('Legal', 'portrait');
        // $dompdf->render();
        // $dompdf->stream($query->referenceId .'.pdf', ["Attachment" => false]);
    }

    public function getScannedCode($id){
        
        //Data Gather 
        $query = $this->reqModel->getDetails(["id" => $id]);
        $query->requisitioner = $this->reqModel->getRequisitioner(["id" => $query->createdBy]);

        $performer = [];
        $approver = [];
        $process = explode(",", $query->performedBy);
        $approve = explode(",", $query->approveBy);
        foreach($process as $key => $value) {
            $qry = $this->reqModel->getUserSignature(["id" => $value]);
            array_push($performer, $qry);
        }
        foreach($approve as $key => $value) {
            $aqry = $this->reqModel->getUserSignature(["id" => $value]);
            array_push($approver, $aqry);
        }

        $query->performedBy = $performer;
        $query->approveBy = $approver;

        $query->verifiedBy = $this->reqModel->getUserSignature(["id" => $query->verifiedBy]);
        $query->encodedBy = $this->reqModel->getUserSignature(["id" => $query->encodedBy]);

        // echo "<pre>";
        // print_r($query);
        // exit();

        // QR Code Implementation
        $writer = new PngWriter();
        // Create QR code
        $qrLink = base_url("index.php/mlrs/api/v1/generate/scan/code/". $id);
        $qrCode = QrCode::create($qrLink)
        ->setEncoding(new Encoding('UTF-8'))
        ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
        ->setSize(300)
        ->setMargin(10)
        ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
        ->setForegroundColor(new Color(0, 0, 0))
        ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);

        // header('Content-Type: '.$result->getMimeType());
        // echo $result->getString();
        // echo "<br/>";

        $qrUri = $result->getDataUri();

        $query->qrCode = $qrUri;

        // Generate the PDF
        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml(view('test_result/result', (array)$query));
        $dompdf->setPaper('Legal', 'portrait');
        $dompdf->render();
        $dompdf->stream($query->referenceId .'.pdf', ["Attachment" => false]);

        

    }

    public function downloadForm(){

        $data = $this->request->getJSON();
        $downloadLink = base_url("index.php/mlrs/api/v1/generate/caseInvestigation/". $data->appId);

        $response = [
            "urlLink" =>  $downloadLink
        ];
        
        return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));

    }

    public function generateCIF($id){

        $query = $this->reqModel->getDetails(["id" => $id]);
        $query->requisitioner = $this->reqModel->getRequisitioner(["id" => $query->createdBy]);

        // echo "<pre>";
        // print_r($query);
        // exit();

        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml(view('test_result/cif-form', (array)$query));
        $dompdf->setPaper('Legal', 'portrait');
        $dompdf->render();
        $dompdf->stream('CIF_'. $query->referenceId .'.pdf', ["Attachment" => true]);

        

    }

    public function cifReports(){

        $list = [];
        $data = $this->request->getJSON();

        $paramas = [
            'status' => $data->status,
            'dateFrom' => $data->dateFrom,
            'dateTo' => $data->dateTo,
        ];
        
        $result = $this->reqModel->getCIFReportRange($paramas);

        foreach ($result as $key => $value) {
            $interviewInfo = json_decode($value->interviewForm);
            $patientInfo = json_decode($value->patientForm);
            $caseInfo = json_decode($value->caseForm);
            $resultInfo = json_decode($value->testResult);

            $list['list'][$key] = [
                "referenceId" => $value->referenceId,
                "specimenId" => $resultInfo->accessionNumber,
                "lastName" => $patientInfo->lastName,
                "firstName" => $patientInfo->firstName .''. $patientInfo->suffix,
                "middleName" => $patientInfo->middleName,
                "birthDate" => date('m/d/Y', strtotime($patientInfo->birthDate)),
                "sex" => $patientInfo->sex,
                "province" => $patientInfo->addressesDetails[0]->province,
                "municipality" => $patientInfo->addressesDetails[0]->municipality,
                "barangay" => $patientInfo->addressesDetails[0]->barangay,
                "street" => $patientInfo->addressesDetails[0]->houseNo .' '. $patientInfo->addressesDetails[0]->street,
                "facility" => $interviewInfo->drUnit,
                "dateOnset" => $caseInfo->dateOfOnset,
                "dateOfCollection" => date('m/d/Y', strtotime($resultInfo->dateCollected)),
                "dateOfReceipt" => date('m/d/Y', strtotime($resultInfo->dateReceipt)),
                "dateOfRelease" => $resultInfo->dateTimeResult,
                "specimenType" => $resultInfo->specimenType,
                "specimenNumber" => '1ST',
                "results" => $resultInfo->testResult,
                "remarks" => $value->resultRemarks,
                "contact" => $patientInfo->addressesDetails[0]->cellNumber,
                "healthStatus" => $caseInfo->healthStatus,
                "rof" => '',
                "typeOfTest" => 'RT-PCR',
                "classification" => $caseInfo->caseClassification,
                "age" => $patientInfo->age,
                "ns1" => $resultInfo->ctValue1,
                "ns2" => $resultInfo->ctValue2,
                "finalResult" => $resultInfo->testResult == 'SARS-CoV-viral RNA Detected' ? 'POSITIVE' : 'NEGATIVE',
            ];
        }

        if($result){
            return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($list));
        } else {
            $response = [
                'title' => 'Error',
                'message' => 'No Data Found'
            ];

            return $this->response
                    ->setStatusCode(404)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));
        }

    }

    
    

}