<?php namespace App\Controllers;

use CodeIgniter\HTTP\IncomingRequest;
use App\Models\RequestModel;
use App\Models\AuthModel;
use \Firebase\JWT\JWT;

class Dashboard extends BaseController
{
    public function __construct(){
        //Models
        $this->reqModel = new RequestModel();
        $this->authModel = new AuthModel();
    }

    public function getDashboard(){

        $list = [];
        $positive = 0;
        $negative = 0;
        $reruns = 0;
        $invalids = 0;
        $payload = $this->request->getJSON();

        if($payload->branch != 1){
            //All Dashboard from branch display in Main branch
            $query = $this->reqModel->getDashboardResultsBranch(['branchId' => $payload->branch]);
        } else {
            //here is the per branch Dashboard
            $query = $this->reqModel->getDashboardResults();
        }

        
        foreach ($query as $key => $value) {

            //Getting the positive and negative
            $data = json_decode($value->testResult, true);
            if(!empty($data)){
                if($data['testResult'] == 'SARS-CoV-viral RNA Detected'){
                    $positive += 1;
                } else if ($data['testResult'] == 'SARS-CoV-viral RNA: Not Detected'){
                    $negative += 1;
                }
            }

            //Getting the Rerun
            if($value->isRerun == 1 && $value->status == 'MLIS001'){
                $reruns += 1;
            }

            //Getting the Invalids
            if($value->status == 'MLIS00D'){
                $invalids += 1;
            }



        }


        $list = [
            'activeCases' => sizeof($query),
            'totalPositive' => $positive,
            'totalNegative' => $negative,
            'totalRerun' => $reruns,
            'totalInvalids' => $invalids,
        ];

        if($list){
            return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($list));
        } else {
            $response = [
                'title' => 'Error',
                'message' => 'No Data Found'
            ];

            return $this->response
                    ->setStatusCode(404)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));
        }

    }

    public function getMonthlyResult(){

        $list = [];
        $first_second = date('m-01-Y');
        $last_second  = date('m-t-Y');
        $payload = $this->request->getJSON();

        if($payload->branch != 1){
            //All Dashboard from branch display in Main branch
            $query = $this->reqModel->getDashboardResultsBranch(['branchId' => $payload->branch]);
        } else {
            //here is the per branch Dashboard
            $query = $this->reqModel->getDashboardResults();
        }

        foreach ($query as $key => $value) {
            //Getting the positive and negative 
            $data = json_decode($value->testResult, true);
            if(!empty($data)){
                // if($data['testResult'] == 'SARS-CoV-viral RNA Detected'){
                //     $positive += 1;
                // } else if ($data['testResult'] == 'SARS-CoV-viral RNA: Not Detected'){
                //     $negative += 1;
                // }
                if($value->status == 'MLIS00X'){
                    $list['data'][$key] = $value;
                } 
                
            }

           


        }


        // $res = [
        //     'first' => $first_second,
        //     'second' => $last_second
        // ];

        print_r($list);
        exit();

    }
    

}