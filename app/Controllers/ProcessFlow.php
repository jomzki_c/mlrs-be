<?php namespace App\Controllers;

use CodeIgniter\HTTP\IncomingRequest;
use App\Models\ProcessFlowModel;
use App\Models\RequestModel;
use App\Models\AuthModel;
use App\Models\HistoryModel;

class ProcessFlow extends BaseController
{

    public function __construct(){
        $this->processModel = new ProcessFlowModel();
        $this->applicationModel = new RequestModel();
        $this->authModel = new AuthModel();
        $this->historyModel = new HistoryModel();
    }

    public function getProcessStatus(){

        //Get API Request Data from NuxtJs
        $data = $this->request->getJSON(); 

        //Select Query for finding User Information
        $status = $this->processModel->where(['status' => $data->status])->get()->getRow();

        if($status){

            $status->listAction = explode("|", $status->listAction);
            $status->nextStep = explode("|", $status->nextStep);
            $status->description = explode("|", $status->description);
            $status->currentStatus = $status->currentStatus == '' ? [] : explode("|", $status->currentStatus);
            // print_r(json_encode($status[0]));
            return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($status));
        } else {
            $response = [
                'title' => 'System Error',
                'message' => 'Please contact administrator.'
            ];

            return $this->response
                    ->setStatusCode(404)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));
        }
    }  

    public function updateProcessStatus(){

        //Get API Request Data from NuxtJs
        $data = $this->request->getJSON(); 

        //GET the data of the user
        $userData = $this->authModel->where('id', $data->user)->get();
        $userData = $userData->getRow();

        //Variables
        $where = ['id' => $data->applicationId];


        $setData = [
            'status' => $data->nextStatus,
            'userId' => $this->ifHasUser($data->nextStatus) ? $data->user : 0,
            'statusDescription' => $data->curStatus,
            'testResult' => json_encode($data->testResult),
            'performedBy' => $data->performedBy,
            'verifiedBy' => $data->verifiedBy,
            'encodedBy' => $data->encodedBy,
            'approveBy' => $data->approveBy,
            'isRerun' => isset($data->rerun) ? 1 : 0,
        ];

        
        // $history = [
        //     'applicationId' => $data->applicationId,
        //     'requestData' => json_encode($data),
        //     'actionStatus' => str_replace(['<user>'], [$userData->firstName .' '. $userData->lastName .' '. $userData->suffix], $data->action),
        //     'createdBy' => $data->user,
        // ];

        // print_r($setData);
        // exit;

        //Update Query for finding User Information
        $updateApp = $this->applicationModel->updateApplication($where, $setData);


        if($updateApp){
            //save application history action
            // $this->historyModel->insert($history);

            //Return to user
            $response = [
                'title' => 'Update Specimen Status',
                'message' => 'Your data successfully updated!'
            ];

            return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));
        } else {
            $response = [
                'title' => 'Update Request Status',
                'message' => 'Your application failed to update!'
            ];

            return $this->response
                    ->setStatusCode(400)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));
        }

        
    } 

    function ifHasUser($status){

        if($status == 'MLIS002' || $status == 'MLIS004' || $status == 'MLIS006' || $status == 'MLIS008'){
            return true;
        } 

        return false;
    }

    // function addToPerformer($status, $user){

    //     $performedBy = ;

    //     if($status == 'MLIS002' || $status == 'MLIS004' || $status == 'MLIS006' || $status == 'MLIS008'){
            
    //     } 

    //     return false;
    // }

}