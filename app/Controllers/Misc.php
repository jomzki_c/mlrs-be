<?php namespace App\Controllers;

use CodeIgniter\HTTP\IncomingRequest;
use App\Models\MiscModel;

class Misc extends BaseController
{

    public function __construct(){
        $this->miscModel = new MiscModel();
    }

    public function getUserTypes(){

        //Select Query for finding User Information

        $categories = [];
        $categories['list'] = $this->miscModel->getTypeList();

        //Set Api Response return to the FE
        if($categories){
            //Update
            return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($categories));
        } else {
            $response = [
                'message' => 'No Data Found'
            ];

            return $this->response
                    ->setStatusCode(404)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));
        }
        
    }   

    public function getBranches(){

        //Select Query for finding User Information
        $unitareas = [];
        $unitareas['list'] = $this->miscModel->getBranchList();

        //Set Api Response return to the FE
        if($unitareas){
            //Update
            return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($unitareas));
        } else {
            $response = [
                'message' => 'No Data Found'
            ];

            return $this->response
                    ->setStatusCode(404)
                    ->setContentType('application/json')
                    ->setBody(json_encode($response));
        }
        
    }   

    public function getAddress($addCode){
        $json = file_get_contents(dirname(__FILE__) . '\addresses.json');
        $obj  = json_decode($json);
        $subjects = [
            "address" => $obj,
            "code" => $addCode
        ];

        $all = array_map(function($el){

            // if($code == 'province'){
            //     return $el->province;
            // } else {
            //     return $el2;
            // }

            return $el->code;
            
        }, $subjects);

        // if($addCode == 'province'){
        //     $all = array_unique($all);
        // }


        return $this->response
                    ->setStatusCode(200)
                    ->setContentType('application/json')
                    ->setBody(json_encode($all));
    }

}